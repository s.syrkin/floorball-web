const firebaseConfig = {
    development: {
        apiKey: "AIzaSyAWRuCIuCQyWs0RrPIxWa3DZec8YH0kkno",
        authDomain: "floorball-b5840.firebaseapp.com",
        databaseURL: "https://floorball-b5840.firebaseio.com",
        projectId: "floorball-b5840",
        storageBucket: "floorball-b5840.appspot.com",
        messagingSenderId: "1090032152613"
    },
    production: {
        apiKey: "AIzaSyAy29_IMomdhdo6T78SRkuuUipaZDlQOMc",
        authDomain: "floorball-prod.firebaseapp.com",
        databaseURL: "https://floorball-prod.firebaseio.com",
        projectId: "floorball-prod",
        storageBucket: "floorball-prod.appspot.com",
        messagingSenderId: "143696099640"
    }
};

export default firebaseConfig;